# Tips and Tricks

This file gathers various tips and tricks to try when your vmbuilder vm isn't working. This involve problems building as well as problems encountered while it's running.

The [training presentation](https://docs.google.com/presentation/d/1gGKBQ38ReO2xSQ4SLzSta-2KhoMcSW_n4rk1tbY0AKU/edit) can be helpful.

## Things to try

These are in no particular order. You need do these things as root, i.e. `sudo su`.

### When things in general are funky

This can help in some cases when the node.js apps are in a less-than-ideal state.

```
ls /etc/init | grep nodejs | cut -d. -f1 | for service in `xargs`; do stop ${service}; start ${service}; done
```

### If squirrel is returning 401

If squirrel is returning 401, reset the time.

```
service ntpd stop; ntpdate -v time.nist.gov; service ntpd start;
```

If this fails, or returns "server unreachable" or something along those lines, do the following:

```
service ntpd stop
```

Then, repeat the following command until it no longer gives that error:

```
ntpdate -v time.nist.gov
```

Then start the service back up again:
```
service ntpd start
```

### If search seems unreachable on your dev box

Edit your `etc/resolve.conf` file's nameserver to `8.8.8.8`.


### Stop Puppet from resetting branches


In the following file:
```
/etc/facter/facts.d/orisfacts.yaml
```

add

```
enforce_git_refs: false
```

This fact makes it so the appserver module won't change app git source but puppet will otherwise change things such as starting your services and pushing updates to Puppet.

 This method is preferable to setting noop to true if your only desire is stopping Puppet from resetting branches. This way, you will still recieve any updates pushed by DevOps and the Puppet scripts will launch everything correctly on boot, you will just not have your branches reset every time.

 An alternative that will halt the system from making ANY changes without you doing them is the following:

```
puppet config set noop true
```

### You see an unexpected shibboleth error page

```
service shibd restart
```

### When the vm's web_url is unreachable

Restart nginx

```
service nginx restart
```

### Connecting to the devbox URL gives error not listed here

Try restarting ALL the services (shibd, nginx, supervisord), not just nginx or shibd:

`
service shibd stop; service shibd start; service nginx stop; service nginx start; service supervisord stop; service supervisord start;
`

### Sometimes handy

Do a puppet run. This is useful as a "last resort" when nothing else listed here works. It essentially re-runs the puppet command that happens when you first build a dev box, and should fix a lot of errors that can happen when tinkering around.


```
puppet agent -t
```

## Things to Know

Verions numbers matter. You need Vagrant 1.7.4 and Virtualbox not greater than 5.0.x.
